from keras.preprocessing.image import img_to_array
import imutils
import cv2
from keras.models import load_model
import numpy as np
from PIL import Image
import os

import glob
from tqdm import tqdm_notebook
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
import tensorflow as tf
from tensorflow.python.keras.backend import set_session
import keras
import pickle
keras.backend.clear_session()
EMOTIONS = ["angry" ,"disgust","scared", "happy", "sad", "surprised",
 "neutral", "attention"]

from sklearn.ensemble import RandomForestClassifier
model = pickle.load(open("RFC_little_set.pkl", 'rb'))
#global graph
#graph = tf.get_default_graph()
#sess = tf.Session()#config=tf_config)

def load_models(detection_model_path = '/home/ubuntu/Emotion-recognition/haarcascade_files/haarcascade_frontalface_default.xml', 
                emotion_model_path = '/home/ubuntu/Emotion-recognition/models/_mini_XCEPTION.102-0.66.hdf5'):
    face_detection = cv2.CascadeClassifier(detection_model_path)
    emotion_classifier = load_model(emotion_model_path, compile=False)
    return face_detection, emotion_classifier

def load_img(fn):
    #frame = cv2.imread(fn)
    #try:
    #    print(1)
    #    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    #except:
    #    print(2)
    gray = cv2.imread(fn ,0)
    faces = face_detection.detectMultiScale(gray,
                                            scaleFactor=1.1,
                                            minNeighbors=4,
                                            minSize=(30,30),
                                            flags=cv2.CASCADE_SCALE_IMAGE)
    if len(faces)==0:
        #pass
        print('no face')
        return None
        img = gray
    else:
        x, y, w, h = faces[0]
        img = gray[y:y+h, x:x+w]

    roi = cv2.resize(img, (64, 64))
    roi = roi.astype("float") / 255.0
    roi = img_to_array(roi)
    roi = np.expand_dims(roi, axis=0)
    return roi

face_detection, emotion_classifier = load_models()
#emotion_classifier._make_predict_function()

def _predict(img):
    lst = emotion_classifier.predict(img)[0]#, batch_size=1, verbose=1)[0]
    feats = np.array(lst)
    if all(feats<=0):
        lst = np.array(list(lst)+[-1])
    else:
        p = model.predict_proba(feats.reshape(1, -1))
        p[:, 0] = p[:, 0] * 5
        p = np.argmax(p, -1)[0]
        lst = np.array(list(lst)+[p])
       
    rtrn_dct = {k:v for k,v in zip(EMOTIONS, lst)}
    return rtrn_dct

def predict(fn):
    img = load_img(fn)
    if img is None:
        return {k:-0.1 for k in EMOTIONS}
    else:
        return _predict(img)
 
import pickle
if __name__ == "__main__":
    while True:
        files = os.listdir('local_images')
        for fn in files:
           res = predict('local_images/'+fn)
           pickle.dump(res, open(f'local_images_pickle/{fn}.pickle', 'wb'))
        if os.path.exists('local_images/'+fn):
            os.remove('local_images/'+fn)
