import pymysql

sql_hostname = 'localhost'
sql_username = 'db_user'
sql_password = 'P@s$w0rd123!'
sql_main_database = 'db_sdk'
sql_port = 3306

base_query = """INSERT INTO emotions (user_id, feat_angry, feat_disgust, feat_scared, 
                        feat_happy, feat_sad, feat_surprised, feat_neutral, feat_attention)
                values('{user_id}', {feat_angry}, {feat_disgust}, {feat_scared},
                       {feat_happy}, {feat_sad}, {feat_surprised}, {feat_neutral}, {feat_attention})"""

creation_query = """
                CREATE TABLE IF NOT EXISTS emotions (
                    id INT AUTO_INCREMENT PRIMARY KEY,
                    user_id VARCHAR(255),
                    event_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                    feat_angry FLOAT,
                    feat_disgust FLOAT,
                    feat_scared FLOAT,
                    feat_happy FLOAT,
                    feat_sad FLOAT,
                    feat_surprised FLOAT,
                    feat_neutral FLOAT,
                    feat_attention FLOAT) ENGINE = INNODB;
                """

example_json = {'user_id': "padamenko",
                'feat_event_timestamp': 1541273,
                'feat_angry': 0.6461246230761835,
                'feat_disgust': 0.9978052913375299,
                'feat_scared': 0.19705943939457204,
                'feat_happy': 0.1001144631701405,
                'feat_sad': 0.9255482420449098,
                'feat_surprised': 0.8738320693250398,
                'feat_neutral': 0.2992323322034748,
                'feat_attention': 0.333
                }


def insert_into_database(json):

    conn = pymysql.connect(host=sql_hostname, user=sql_username,
                           password=sql_password, db=sql_main_database,
                           charset='utf8', port=sql_port)

    user_id = json['user_id']

    feat_angry = json['feat_angry']
    feat_disgust = json['feat_disgust']
    feat_scared = json['feat_scared']
    feat_happy = json['feat_happy']
    feat_sad = json['feat_sad']
    feat_surprised = json['feat_surprised']
    feat_neutral = json['feat_neutral']
    feat_attention = json['feat_attention']

    with conn:
        cur = conn.cursor()
        cur.execute(base_query.format(user_id=user_id, feat_angry=feat_angry, feat_disgust=feat_disgust,
                                      feat_scared=feat_scared, feat_happy=feat_happy, feat_sad=feat_sad,
                                      feat_surprised=feat_surprised, feat_neutral=feat_neutral,
                                      feat_attention=feat_attention))

insert_into_database(example_json)

