
from flask import Flask,request,jsonify
import numpy as np
import cv2
#import tensorflow as tf
import base64
from base64 import b64decode
from db_insert import insert_into_database

import pickle
import time
from flask_cors import CORS, cross_origin

#from utils import predict
#import tensorflow as tf
app = Flask(__name__)
CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

#graph = tf.get_default_graph()

#print(predict("image.png"))

@app.route('/run')
@cross_origin()
def hello_world():
    return open('index.html', 'r').read()


@app.route('/start',methods=['GET'])
@cross_origin()
def test():
    return open('enter.html', 'r').read()#"hello world!"i

import datetime
@app.route('/submit',methods=['POST'])
@cross_origin()
def submit():
    image = request.form
    data_url = image['image']

    header, encoded = data_url.split(",", 1)
    data = b64decode(encoded)
    user_id = image.get('user_id', 'anonym')

    with open(f"local_images/{hash(user_id)}.png", "wb") as f:
        f.write(data)
    time.sleep(0.5)

    try:
        res = pickle.load(open(f'local_images_pickle/{hash(user_id)}.png.pickle', 'rb'))
    except: 
        return ""

    json = {'feat_'+key:val for key, val in res.items()}
    #print(json)
    if 'feat_attention' not in json:
        json['feat_attention'] = 0.2
    json['user_id'] = user_id#request.args.get('user_id', 'anonym')
    json['feat_event_timestamp'] = int(datetime.datetime.now().timestamp())
    print(json)
    insert_into_database(json)
    return ""

if __name__ == '__main__':
    app.run(host='127.0.0.1', debug=True, port="5000")

